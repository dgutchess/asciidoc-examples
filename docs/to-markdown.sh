#!/bin/sh

# To install: sudo apt install asciidoctor pandoc

asciidoctor -b docbook dev-guide.adoc
pandoc -f docbook -t markdown_strict dev-guide.xml -o dev-guide.md

